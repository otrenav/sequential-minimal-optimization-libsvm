
| [Website](http://links.otrenav.com/website) | [Twitter](http://links.otrenav.com/twitter) | [LinkedIn](http://links.otrenav.com/linkedin)  | [GitHub](http://links.otrenav.com/github) | [GitLab](http://links.otrenav.com/gitlab) | [CodeMentor](http://links.otrenav.com/codementor) |

---

# SMO LIBSVM

- Omar Trejo
- November, 2105

Sequential Minimal Optimization (SMO) LIBSVM implementation I developed by using
Matlab.

This implementation is based on:

- [Fan, Chen & Lin - Working set selection using second order information for training support vector machines (2005)](http://www.jmlr.org/papers/volume6/fan05a/fan05a.pdf)

---

> "The best ideas are common property."
>
> —Seneca
